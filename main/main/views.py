from django.shortcuts import render
from django.views.generic import TemplateView

def index(request):
	return render(
        request,
        'main/index.html',
    )

def cv(request):
	return render(
        request,
        'main/cv.html',
    )
def github(request):
	return render(
        request,
        'main/github.html',
    )